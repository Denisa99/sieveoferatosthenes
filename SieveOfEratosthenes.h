#pragma once
#include <iostream>
#include <fstream>
#include <random>
#include <chrono>
using namespace std;
using namespace std::chrono;
#define noNumbers 50
#define startInterval 2500
#define endInterval 4000

void SieveOfEratosthenes(int n);
