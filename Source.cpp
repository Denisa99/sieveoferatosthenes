#include "SieveOfEratosthenes.h"

int main(int argc, char* argv[])
{
	//Generate random numbers in the input file 
	ofstream outdata; // outdata is like cin
	random_device rd; // obtain a random number from hardware
	mt19937 gen(rd()); // seed the generator
	uniform_int_distribution<> distr(startInterval, endInterval); // define the range

	string filename("input.txt");
	int number;

	outdata.open("input.txt"); // opens the file
	if (!outdata) { // file couldn't be opened
		cerr << "Error: file could not be opened" << endl;
		exit(1);
	}

	for (int i = 0; i < 10; i++) {
		outdata << distr(gen) << " ";
	}
	outdata.close();

	//Read the input numbers
	ifstream input_file(filename);
	if (!input_file.is_open()) {
		cerr << "Could not open the file - '"
			<< filename << "'" << endl;
		return EXIT_FAILURE;
	}

	auto start = high_resolution_clock::now();

	while (input_file >> number) {
		SieveOfEratosthenes(number);
	}

	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);

	cout << "\nExecution time: "
		<< duration.count() << " microseconds" << endl;
	input_file.close();

	return 0;
}
