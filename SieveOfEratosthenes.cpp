#include "SieveOfEratosthenes.h"

//C++ method used to calculate all prime numbers small or equal than a given number, n.
void SieveOfEratosthenes(int n)
{
	//use a boolean array in order to get all the prime numbers
	bool* prime = new bool[n + 1];

	//initialize all elements of the array with true
	memset(prime, true, sizeof(prime));

	for (int i = 2; i * i <= n; i++) {
		if (prime[i]) {
			// Update all multiples of i
			for (int j = i * i; j <= n; j += i) {
				//if value is changed, then the corresponding number isn't prime
				prime[j] = false;
			}
		}
	}

	// Print the prime numbers list
	cout << "\nPrime numbers for " << n << ": ";
	for (int i = 2; i <= n; i++) {
		if (prime[i]) {
			cout << i << " ";
		}
	}

}